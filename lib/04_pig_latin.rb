$vowels = ["a", "e", "i", "o", "u"]

$bi_cluster = ["sh", "ch", "sq", "th", "qu", "br"]

$tri_cluster = ["sch", "thr", "squ"]

def translate(sentence)
  words = sentence.downcase.split(" ")
  words.map! do |word|
    if $vowels.none? { |vowel| word[0] == vowel }
      word = cluster_buster(word)
    end
    word + "ay"
  end
  words.join(" ")
end

#helps #translate break up initial multiple consonants
def cluster_buster(word)
  consonants = ""
  if $tri_cluster.any? { |cluster| word.start_with?(cluster) }
    consonants = word.slice!(0..2)
  elsif $bi_cluster.any? { |cluster| word.start_with?(cluster) }
    consonants = word.slice!(0..1)
  else
    consonants = word.slice!(0)
  end
  word += consonants
end
