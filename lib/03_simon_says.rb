$LITTLE_WORDS = ["a", "an", "the", "but", "or", "for", "nor", "on",
                 "at", "to", "from", "by", "and", "over"]

def echo(string)
  string.to_s
end

def shout(string)
  string.upcase
end

def repeat(string, repeat = 2)
  result = string.to_s
  (1...repeat).each { result += " " + string.to_s }
  return result
end

def start_of_word(word, letter_count)
  word[0, letter_count]
end

def first_word(sentence)
  sentence.split[0]
end

def titleize(sentence)
  words = sentence.split
  result = []
  count = 0
  words.each do |x|
    if $LITTLE_WORDS.none? { |word| x == word } || count == 0
      result[count] = x.capitalize
    else
      result[count] = x
    end
    count += 1
  end
  result.join(" ")
end
