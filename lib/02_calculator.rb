def add(summand_1, summand_2)
  summand_1 + summand_2
end

def subtract(subtrahend, minuend)
  subtrahend - minuend
end

def sum(num_array)
  sum = 0
  num_array.each { |x| sum += x }
  return sum
end

def multiply(num_array)
  product = 1
  num_array.each { |x| product *= x }
  return product
end

def power(base, exp)
  base**exp
end

def factorial(number)
  product = 1
  range = 1..number
  range.each { |x| product *= x }
  return product
end
